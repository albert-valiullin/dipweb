<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head lang="en">
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE-edge">
        <meta name="viewport" content="width-device-width, initial-scale=1">
        <title>Main page</title>
        <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.css"/>
        <link rel="stylesheet" href="resources/css/memo.css"/>
        <script src="resources/angular.js"></script>
        <script src="resources/ui-bootstrap-tpls-0.13.3.js"></script>
        <script src="resources/app.js"></script>
        <!-- Controllers -->
        <script src="resources/controllers/MemoListController.js"></script>
        <!-- Directives -->
        <script src="resources/directives/listType.js"></script>
    </head>
    <body ng-app="MemoApp">

        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" ng-init="navCollapsed = true" ng-click="navCollapsed = !navCollapsed">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Brand</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" ng-class="!navCollapsed && 'in'">

                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Link</a></li>
                    <li><a href="#">Link</a></li>

                    <li dropdown>
                        <a href="#" dropdown-toggle >Dropdown <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                            <li><a href="#">One more separated link</a></li>
                        </ul>
                    </li>

                </ul>
                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">${username}</a></li>
                    <li dropdown>
                        <a href="#" dropdown-toggle>Dropdown <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
            </div>
        </nav>

        <div class="container-fluide">
            <h1>Memos</h1>
            <div class="col-md-12">
            <div class="memos row" ng-controller="MemoListController">
                <div class="memo-listing col-md-3" ng-repeat="memo in memos">
                    <div class="memo-head">
                        <h3>{{memo.name}}</h3>
                    </div>
                    <div class="point-list">
                        <list-type ng-init="memoType = memo.type % 2 != 0 ? 'ol': 'ul'">
                            <li ng-repeat="item in memo.content">
                                <input ng-if="memo.type > 1" id="{{memo.id+'_'+$index}}" type="checkbox" ng-model="item.done" ng-click="done(item, $index, memo)"/>
                                <label ng-class="{done: item.done}" for="{{memo.id+'_'+$index}}">{{item.text}}</label>
                            </li>
                        </list-type>
                    </div>
                    <p class="info">{{memo.owner.name}} | {{memo.startDate | date}}</p>
                </div>
            </div>
            </div>
        </div>
    </body>
</html>