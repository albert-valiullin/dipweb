/**
 * Created by Admin on 03.09.2015.
 */
app.controller('MemoListController', ['$scope', function($scope) {
    $scope.done = function(item, idx, memo) {
        if (memo.type === 3) {
            if (item.done) {
                if (idx !== 0 && !memo.content[idx - 1].done) {
                    // aborted - uncheck
                    item.done = !item.done;
                    alert("You should pass previous steps");
                    return;
                } else {
                    // everything ok - check
                    /*if (idx === memo.content.length-1) {
                        alert("Congratulations");
                    }*/
                }
            } else {
                if (!memo.content[idx+1] || !memo.content[idx+1].done || confirm("Are you sure? Next steps will be unchecked!")) {
                    for (var i = idx+1; i < memo.content.length; i++) {
                        memo.content[i].done = false;
                    }
                    // everything ok - uncheck
                } else {
                    // cancelled - check
                    item.done = !item.done;
                    return;
                }
            }
        }

        if (memo.type >= 2 && item.done) {
            var wellDone = true;
            memo.content.forEach(function(el){
                wellDone &= el.done;
            });
            if (wellDone) {
                alert("Congratulations");
            }
        }
    };
    $scope.memos = [
        {
            id: 1,
            name: 'First Memo (Unordered point)',
            content: [
                {text: 'Point 1', importance: 0},
                {text: 'Point 2', importance: 0},
                {text: 'Point 3', importance: 0},
                {text: 'Point 4', importance: 0},
                {text: 'Point 5', importance: 0}
            ],
            comment: 'No comment',
            isPrivate: true,
            owner:  {
                id: 1,
                name: 'Admin'
            },
            type: 0,
            startDate: new Date(),
            color: 0,
            category: {
                id: 0,
                name: 'Not Specified'
            }
        },
        {
            id: 2,
            name: '2d Memo (Ordered point)',
            content: [
                {text: 'Point 1', importance: 0},
                {text: 'Point 2', importance: 0},
                {text: 'Point 3', importance: 0},
                {text: 'Point 4', importance: 0},
                {text: 'Point 5', importance: 0}
            ],
            comment: 'No comment',
            isPrivate: true,
            owner:  {
                id: 1,
                name: 'Admin'
            },
            type: 1,
            startDate: new Date(),
            color: 0,
            category: {
                id: 0,
                name: 'Not Specified'
            }
        },
        {
            id: 3,
            name: 'Third Memo (Unordered checklist)',
            content: [
                {text: 'Check 1', importance: 0, done: false},
                {text: 'Check 2', importance: 0, done: false},
                {text: 'Check 3', importance: 0, done: false},
                {text: 'Check 4', importance: 0, done: false},
                {text: 'Check 5', importance: 0, done: false}
            ],
            comment: 'No comment',
            isPrivate: true,
            owner:  {
                id: 1,
                name: 'Admin'
            },
            type: 2,
            startDate: new Date(),
            color: 0,
            category: {
                id: 0,
                name: 'Not Specified'
            }
        },
        {
            id: 4,
            name: '4th Memo (Ordered checklist)4th Memo (Ordered checklist)',
            content: [
                {text: 'Step 1', importance: 0, done: false},
                {text: 'Step 2', importance: 0, done: false},
                {text: 'Step 3', importance: 0, done: false},
                {text: 'Step 4', importance: 0, done: false},
                {text: 'Step 5', importance: 0, done: false}
            ],
            comment: 'No comment',
            isPrivate: true,
            owner:  {
                id: 1,
                name: 'Admin'
            },
            type: 3,
            startDate: new Date(),
            color: 0,
            category: {
                id: 0,
                name: 'Not Specified'
            }
        }
    ];
}]);