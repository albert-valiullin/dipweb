package ru.kpfu.itis.diplom.dao;

import ru.kpfu.itis.diplom.model.User;

/**
 * Created by Admin on 02.08.2015.
 */
public interface UserDAO extends DAO<User, Integer>{
    User getByUsername(String username);
}
