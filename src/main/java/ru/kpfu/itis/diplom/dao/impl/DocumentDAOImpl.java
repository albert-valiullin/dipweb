package ru.kpfu.itis.diplom.dao.impl;

import org.springframework.stereotype.Repository;
import ru.kpfu.itis.diplom.dao.DocumentDAO;
import ru.kpfu.itis.diplom.model.Document;

/**
 * Created by Admin on 07.02.2016.
 */
@Repository
public class DocumentDAOImpl extends DAOImpl<Document, Integer>implements DocumentDAO {
}
