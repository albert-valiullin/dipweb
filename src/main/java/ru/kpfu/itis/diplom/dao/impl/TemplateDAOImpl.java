package ru.kpfu.itis.diplom.dao.impl;

import org.springframework.stereotype.Repository;
import ru.kpfu.itis.diplom.dao.TemplateDAO;
import ru.kpfu.itis.diplom.model.Template;

/**
 * Created by Admin on 02.08.2015.
 */
@Repository
public class TemplateDAOImpl extends DAOImpl<Template, Integer> implements TemplateDAO {
}
