package ru.kpfu.itis.diplom.dao;

import ru.kpfu.itis.diplom.model.OneTimePass;

/**
 * Created by Admin on 07.02.2016.
 */
public interface OneTimePassDAO extends DAO<OneTimePass, Integer> {
}
