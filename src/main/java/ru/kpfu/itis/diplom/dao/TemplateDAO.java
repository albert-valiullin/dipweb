package ru.kpfu.itis.diplom.dao;


import ru.kpfu.itis.diplom.model.Template;

/**
 * Created by Admin on 02.08.2015.
 */
public interface TemplateDAO extends DAO<Template, Integer> {
}
