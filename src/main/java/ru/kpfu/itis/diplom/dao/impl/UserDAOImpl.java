package ru.kpfu.itis.diplom.dao.impl;

import org.springframework.stereotype.Repository;
import ru.kpfu.itis.diplom.dao.UserDAO;
import ru.kpfu.itis.diplom.model.User;

/**
 * Created by Admin on 02.08.2015.
 */
@Repository
public class UserDAOImpl extends DAOImpl<User, Integer> implements UserDAO {
    public User getByUsername(String username) {
        return (User) sessionFactory.getCurrentSession().createQuery("from User u where u.username = :uname").setString("uname", username).uniqueResult();
    }
}
