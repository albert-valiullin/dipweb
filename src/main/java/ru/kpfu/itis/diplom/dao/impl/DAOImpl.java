package ru.kpfu.itis.diplom.dao.impl;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ru.kpfu.itis.diplom.dao.DAO;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by Admin on 03.08.2015.
 */
@Transactional
public class DAOImpl<T, Id extends Serializable> implements DAO<T, Id> {
    // some reflection magic
    private Class<T> entClass = (Class<T>)
            ((ParameterizedType) getClass().getGenericSuperclass())
                    .getActualTypeArguments()[0];

    @Autowired
    protected SessionFactory sessionFactory;

    public void create(T entity) {
        sessionFactory.getCurrentSession().save(entity);
    }

    public T findById(Id id) {
        return (T) sessionFactory.getCurrentSession().get(entClass, id);
    }

    public void update(T entity) {
        sessionFactory.getCurrentSession().update(entity);
    }

    public void delete(T entity) {
        sessionFactory.getCurrentSession().delete(entity);
    }

    public List<T> findAll() {
        return (List<T>) sessionFactory.getCurrentSession().createCriteria(entClass).list();
    }

    public Object fetch(T entity, String methodName) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = entity.getClass().getMethod(methodName);
        entity = (T) sessionFactory.getCurrentSession().merge(entity);
        Hibernate.initialize(method.invoke(entity));
        return method.invoke(entity);
    }
}
