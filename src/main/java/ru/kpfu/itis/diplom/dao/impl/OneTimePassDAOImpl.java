package ru.kpfu.itis.diplom.dao.impl;

import org.springframework.stereotype.Repository;
import ru.kpfu.itis.diplom.dao.OneTimePassDAO;
import ru.kpfu.itis.diplom.model.OneTimePass;

/**
 * Created by Admin on 07.02.2016.
 */
@Repository
public class OneTimePassDAOImpl extends DAOImpl<OneTimePass, Integer> implements OneTimePassDAO {
}
