package ru.kpfu.itis.diplom.dao;

import ru.kpfu.itis.diplom.model.Document;

/**
 * Created by Admin on 07.02.2016.
 */
public interface DocumentDAO extends DAO<Document, Integer> {
}
