package ru.kpfu.itis.diplom.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.diplom.service.UserService;

/**
 * Created by Admin on 19.02.2016.
 */
@Controller
public class DocumentController {
    @Autowired
    private UserService userService;

//    @Autowired
//    private DocumentService documentService;


    @RequestMapping(value = "/docs", method = RequestMethod.GET)
    public String getDocs(Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("username", user.getUsername());
        ru.kpfu.itis.diplom.model.User userModel = userService.getByUsername(user.getUsername());

        System.out.println(userService.fetchDocuments(userModel));
        model.addAttribute("docList", userService.fetchDocuments(userModel));
        return "docs";
    }
}
