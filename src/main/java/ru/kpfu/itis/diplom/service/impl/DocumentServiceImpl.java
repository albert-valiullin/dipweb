package ru.kpfu.itis.diplom.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.diplom.dao.DocumentDAO;
import ru.kpfu.itis.diplom.model.Document;
import ru.kpfu.itis.diplom.service.DocumentService;

import java.util.List;

/**
 * Created by Admin on 08.02.2016.
 */
@Service
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private DocumentDAO documentDAO;

    public void createDocument(Document document) {
        documentDAO.create(document);
    }

    public List<Document> findAll() {
        return documentDAO.findAll();
    }
}
