package ru.kpfu.itis.diplom.service;

import ru.kpfu.itis.diplom.model.Document;
import ru.kpfu.itis.diplom.model.User;

import java.util.List;
import java.util.Set;

/**
 * Created by Admin on 02.08.2015.
 */
public interface UserService {
    void create(User user);
    List<User> findAll();
    Set<Document> fetchDocuments(User user);
    User getByUsername(String username);
}
