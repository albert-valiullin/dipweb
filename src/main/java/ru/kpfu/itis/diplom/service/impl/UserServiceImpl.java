package ru.kpfu.itis.diplom.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.diplom.dao.UserDAO;
import ru.kpfu.itis.diplom.model.Document;
import ru.kpfu.itis.diplom.model.User;
import ru.kpfu.itis.diplom.service.UserService;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Set;

/**
 * Created by Admin on 02.08.2015.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    public void create(User user) {
        userDAO.create(user);
    }

    public List<User> findAll() {
        return userDAO.findAll();
    }

    public Set<Document> fetchDocuments(User user) {
        try {
            return (Set<Document>) userDAO.fetch(user, "getDocuments");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public User getByUsername(String username) {
        return userDAO.getByUsername(username);
    }


}
