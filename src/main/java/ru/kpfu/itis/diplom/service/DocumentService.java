package ru.kpfu.itis.diplom.service;

import ru.kpfu.itis.diplom.model.Document;

import java.util.List;

/**
 * Created by Admin on 08.02.2016.
 */
public interface DocumentService {
    void createDocument(Document document);
    List<Document> findAll();

}
