package ru.kpfu.itis.diplom.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Admin on 05.02.2016.
 */
@Entity
@Table(name = "ONE_TIME_PASS")
public class OneTimePass {
    @Id
    @Column(name = "PASS_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "PASS_PRIV", nullable = false, length = 255)
    private String privateKey;

    @Column(name = "PASS_PUB", nullable = false, length = 255)
    private String publicKey;

    @Column(name = "PASS_CREATED")
    private Timestamp created;

    @Column(name = "PASS_EXPIRED")
    private Timestamp expired;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OneTimePass that = (OneTimePass) o;

        if (!privateKey.equals(that.privateKey)) return false;
        return publicKey.equals(that.publicKey);

    }

    @Override
    public int hashCode() {
        int result = privateKey.hashCode();
        result = 31 * result + publicKey.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "OneTimePass{" +
                "id=" + id +
                ", privateKey='" + privateKey + '\'' +
                ", publicKey='" + publicKey + '\'' +
                ", created=" + created +
                ", expired=" + expired +
                '}';
    }
}
