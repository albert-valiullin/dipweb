package ru.kpfu.itis.diplom.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by Admin on 13.07.2015.
 */
@Entity
@Table(name = "USER")
public class User implements Serializable {
    @Id
    @Column(name = "USER_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "USER_NAME", unique = true, nullable = false, length = 255)
    private String username;

    @Column(name = "USER_EMAIL", unique = true, nullable=false, length = 255)
    private String email;

    @Column(name = "USER_PASS", nullable = false, length = 255)
    private String password;

//    @Column(name = "USER_OAUTH", length = 255)
//    private String oauth;

//    @Column(name = "USER_CREATE_DATE")
//    private Timestamp createDate;

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    private Set<Document> documents;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(Set<Document> documents) {
        this.documents = documents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!username.equals(user.username)) return false;
        return email.equals(user.email);

    }

    @Override
    public int hashCode() {
        int result = username.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
