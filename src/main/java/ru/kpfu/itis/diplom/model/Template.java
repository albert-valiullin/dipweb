package ru.kpfu.itis.diplom.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by Admin on 13.07.2015.
 */
@Entity
@Table(name = "TEMPLATE")
public class Template implements Serializable {
    @Id
    @Column(name = "TEMP_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Template{" +
                "id=" + id +
                '}';
    }

}
