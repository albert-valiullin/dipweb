package ru.kpfu.itis.diplom.model;

import javax.persistence.*;
import java.sql.Blob;
import java.util.Arrays;

/**
 * Created by Admin on 05.02.2016.
 */
@Entity
@Table(name = "DOCUMENT")
public class Document {
    @Id
    @Column(name = "DOC_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "DOC_USER_ID", nullable = false)
    private User owner;

    @Column(name = "DOC_DATA", columnDefinition = "BLOB")
    private byte[] data;

    @Column(name = "DOC_PREV", nullable = false, length = 255)
    private String preview;

    @Column(name = "DOC_SALT", nullable = false, length = 255)
    private String salt;

    @Column(name = "DOC_CTRL", nullable = false, length = 255)
    private String controlPhrase;

    @Column(name = "DOC_ENCR", nullable = false, length = 255)
    private String encryptedPhrase;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getControlPhrase() {
        return controlPhrase;
    }

    public void setControlPhrase(String controlPhrase) {
        this.controlPhrase = controlPhrase;
    }

    public String getEncryptedPhrase() {
        return encryptedPhrase;
    }

    public void setEncryptedPhrase(String encryptedPhrase) {
        this.encryptedPhrase = encryptedPhrase;
    }


    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Document document = (Document) o;

        if (!owner.equals(document.owner)) return false;
        if (!Arrays.equals(data, document.data)) return false;
        if (!preview.equals(document.preview)) return false;
        if (!salt.equals(document.salt)) return false;
        if (!controlPhrase.equals(document.controlPhrase)) return false;
        return encryptedPhrase.equals(document.encryptedPhrase);

    }

    @Override
    public int hashCode() {
        int result = owner.hashCode();
        result = 31 * result + Arrays.hashCode(data);
        result = 31 * result + preview.hashCode();
        result = 31 * result + salt.hashCode();
        result = 31 * result + controlPhrase.hashCode();
        result = 31 * result + encryptedPhrase.hashCode();
        return result;
    }
}
